# README

The Neurorobotics Platform (NRP) is a subproject of the Human Brain Project (HBP), funded by the European Commission (EC). It is distributed as open source under the terms of the GNU Public License version 2, which you can find a copy of in each of our repositories.

Visit our [website](http://www.neurorobotics.net)!

## What is this repository for?

This is the meta-repository for the Neurorobotics Platform. It provides the guide to installation of the Platform from source code.

## Who do I talk to?

To report a bug or request a new feature, please send an email to [neurorobotics-support@humanbrainproject.eu](mailto:neurorobotics-support@humanbrainproject.eu) or, if you have HBP credentials, log in to the [HBP support portal](https://support.humanbrainproject.eu/).

To get support from the developer team, post on our [forum](https://forum.humanbrainproject.eu/c/neurorobotics).

In case you need to contact us for another purpose, please use the [contact form](http://www.neurorobotics.net/contact.html).


## Source code full NRP installation

Follow these guides in order to install NRP from source on your machine:

1. [Installation prerequisites](install_prereq.md)
1. [Installing the NRP from source](source_install.md)
1. [Running the NRP](running_platform.md)
1. [Installation troubleshooting](dev_troubleshooting.md)