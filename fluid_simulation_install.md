Installing the fluid simulation on the NRP
==========================================

The Neurorobotics Platform provides a fluid simulation based on SPlisHSPlasH (https://github.com/InteractiveComputerGraphics/SPlisHSPlasH), coupled with the 
robot simulation. It is not part of the standard distribution. To install it follow these steps:

1. Download our version of SPlisHSPlasH which contains the coupling between the robots and fluids

        cd $HBP
        git clone git@bitbucket.org:hbpneurorobotics/splishsplash.git SPlisHSPlasH

2. Build and install the fluid simulation

        cd $HBP/SPlisHSPlasH
        mkdir build
        cd build
        cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local ..
        make
        make install

This operation will install the necessary plugin into your gazebo plugin folder (usually $HOME/.local/lib/gazebo-{gazebo_version}/plugins), and then you 
can use the fluid simulation in a world definition file.

Finally start your NRP and clone the template fluid experiment