### Supported OS

This installation doc has been tested on **Ubuntu 20.04**. If using a different distribution, you will have to adapt the dependencies without support from us. Installations on **virtual machines**
are often failing due to bad support of gpu drivers, but you can configure NRP to use CPU-rendering instead. This will slow it down, but should work.

**WARNING**

_APT_ and _pip_ are not good friends. If you installed many dist-packages using sudo pip install, you should consider uninstalling them and using the deb packages provided by Ubuntu instead, even if the versions are late. Indeed, the deb packages are tested by Ubuntu and work together. They are the reference. Anything else is installed locally at build time.
So, practically, you should have a `$HOME/.local/lib/python3.8/dist-packages` containing ONLY Nest related stuff: _nest_, _PyNest_, _Topology_ and _ConnPlotter_. If you have other packages, that exist as deb packages, you are strongly advised to pip uninstall them and apt-get install the deb versions instead.
In particular, pip should be the official Ubuntu one, and not be pip installed.

```bash
pip uninstall pip 
sudo apt-get install python3-pip
```

> **_NOTE:_** see known issues in [troubleshooting page](dev_troubleshooting.md).

This documentation helps you install a fully local neurorobotics platform. You can of course choose to install all the backend stuff on one computer and the frontend on your laptop for example also, but this is not described here.

**Very important**: in this setup, NRP is installed in your user space (no root install).
