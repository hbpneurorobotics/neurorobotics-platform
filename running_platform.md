
# Running the Platform

Make sure nginx and haproxy are running fine (restart them : it should output `[ OK ]` for nginx and haproxy) and watch the error_log.

```bash
cle-reverse-proxies
```

Then start the NRP backend by using either

```bash
cle-start
```

You can then connect with your browser:

Open
<http://localhost:9000/#/esv-private>
You will be prompted for credentials: default user is "nrpuser/password". Clone an experiment.

If you want to see experiments that are in "development" maturity (your own for example), add the "?dev" parameter to the url:
<http://localhost:9000/#/esv-private?dev>

## Stopping the Platform

To stop the Platform and kill all processes:

* press CTRL+C in the terminal where you entered `cle-start` and type
  
        cle-kill

# Updating the Platform

This is very easy! Just go to `user-scripts` and run

```bash
cd "$HBP"/user-scripts
git pull --rebase
./update_nrp update --all
./configure_nrp
```

And open a fresh terminal to start your Platform as usual.
If you don't provide the optional ```--all``` argument, only the core python repos will be updated, which is a much shorter build, but might miss changes in 3rd party software like gazebo. So we would recommend that you just use this argument and get a lunch break while it builds.

## Update error?

The `update_nrp` script will exit on error if you have local changes in the repos. So if you do not want to risk a failure after one hour of build, we recommend that you make sure your repos in `$HBP` are clean or changes are stashed or committed.

[issues]: https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/issues
