
# NRP Installation

## 1. Initial set up

1. Create a folder in you home which will contain required 3rd-party software

        mkdir $HOME/.local

2. Create a new directory where you will install all the source code for NRP. We suggest `$HOME/Documents/NRP`. This directory will be referred to in the rest of this document as **$HBP**.<br/>
    Add the `$HBP` environment variable to your bashrc.<br/>
    Add the `$NRP_INSTALL_MODE` variable to your bashrc which will make sure you use the publicly accessible code and build process, **unless you are a core developer and have writing rights to the repos**.

        # add the following to your $HOME/.bashrc file
        export HBP=$HOME/Documents/NRP  # Or whatever installation path you chose
        export NRP_INSTALL_MODE=user  # skip this variable if you are a core NRP developer and want the developer branches of the repos
        
    Source your .bashrc

        source $HOME/.bashrc

3. Apt prerequisites

         sudo apt-get install python3-venv python-is-python3 cmake git build-essential doxygen python3-dev python3-h5py python3-lxml autogen automake libtool build-essential autoconf libltdl-dev libtinyxml-dev libreadline-dev libncurses5-dev libgsl-dev python3-all-dev python3-docopt python3-numpy python3-scipy python3-matplotlib ipython3 libxslt1-dev zlib1g-dev libfreetype-dev python3-opencv ruby libtar-dev libprotoc-dev protobuf-compiler imagemagick libtinyxml2-dev python3-virtualenv libffi-dev uwsgi-plugin-python3 python3-pip cimg-dev libgnuplot-iostream-dev jupyter-notebook xvfb

4. Clone the `user-scripts` repository. This repository includes some helper scripts to ease the installation.

        mkdir -p "$HBP"
        cd "$HBP"
        git clone https://bitbucket.org/hbpneurorobotics/user-scripts.git --branch=master

5. Use the clone-all-repos script from user-scripts to clone all necessary repos at once. The $HBP variable need to be set for this script to work successfully!

        cd "$HBP"/user-scripts
        ./clone-all-repos

6. Empty the `PYTHONPATH` env variable. Add `PYTHONPATH=` to the end of your `$HOME/.bashrc`

        PYTHONPATH=
        echo "PYTHONPATH=" >> ~/.bashrc

7. Set up your environment automatically by adding the following two lines at the Bottom of your `$HOME/.bashrc`

        . "$HBP"/user-scripts/nrp_variables
        . "$HBP"/user-scripts/nrp_aliases

    >**_NOTE:_** Generally, if you want to change any of the variables for any reason, then override the variables in your `$HOME/.bashrc` and do not alter the `nrp_variables` file.

    Then source your bashrc. **You will get some expected errors** after sourcing the `$HOME/.bashrc` **if you're doing a fresh install**. This is due to some missing files that will be instantiated during the installation process.

        source $HOME/.bashrc

## 2. Install ROS

>**_NOTE:_** if you already installed _ROS_ for your Ubuntu from apt-get, then you can skip this section.

1. Add GPG key of ROS to apt

        sudo wget -O - https://raw.githubusercontent.com/ros/rosdistro/master/ros.key | sudo apt-key add -

2. Add apt repository for ROS

        sudo apt-add-repository "deb http://packages.ros.org/ros/ubuntu focal main"

3. Update repositories

        sudo apt-get update

4. Install ROS Noetic.

        sudo apt-get install ros-noetic-desktop-full
        sudo apt-get install ros-noetic-control-toolbox ros-noetic-controller-manager ros-noetic-transmission-interface ros-noetic-joint-limits-interface ros-noetic-rosauth ros-noetic-smach-ros ros-noetic-effort-controllers ros-noetic-async-web-server-cpp ros-noetic-web-video-server python3-rospkg python3-rosdep python3-bson python3-osrf-pycommon python3-catkin-tools

5. Initialize ROS

         sudo rosdep init
         rosdep update

## 3. Gazebo prerequisites

1. **If you have Gazebo 11 already installed**, remove it first. (Remove any other version of Gazebo that you would have installed by yourself)

        sudo apt-get remove --purge gazebo11* ros-noetic-gazebo-msgs ros-noetic-gazebo-plugins ros-noetic-gazebo-ros ros-noetic-gazebo-ros-control ros-noetic-gazebo-ros-pkgs

    Test that `roscore` command is still functioning. If not, consider reinstalling ROS by first removing it with `apt-get remove --purge ros-noetic*` and following the above paragraph again.

2. Add GPG key of Gazebo to apt

        wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add - 

3. Add apt repository for Gazebo

        sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'

4. Update repositories

        sudo apt-get update

5. Install build dependencies

        sudo apt-get install libignition-fuel-tools4-dev libignition-math6-dev libignition-transport8-dev libignition-msgs5-dev libignition-common3-graphics-dev libignition-common3-dev libignition-common3-profiler-dev

6. Install bullet from apt

        sudo apt-get install libbullet-dev

7. Install Gazebo dependencies

    You may require additional libraries to compile gazebo with all its options:

        sudo apt-get install libgts-dev libgdal-dev ruby-ronn xsltproc graphviz-dev

    >***WARNING:*** Check that `$HOME/.local/lib/x86_64-linux-gnu/cmake/gazebo` is in your `$CMAKE_PREFIX_PATH`. If not, you might have forgotten to add `nrp_variables` in your `$HOME/.bashrc`.<br/>
    Further, make sure that `$HOME/.local/lib/x86_64-linux-gnu/pkgconfig` is in your `$PKG_CONFIG_PATH`. Otherwise, you will see errors that later builds will claim they did not find the HBP Gazebo features.

    >***WARNING:*** When you will build the whole NRP later, if gazebo build fails because you miss a number of libs (see end summary from cmake output), just apt-get the required ones, from the _BUILD_ERRORS_ in the cmake step.

    >**_NOTE:_** Check that `$GAZEBO_MODEL_PATH` for example exists. If not, you might have forgotten to add `nrp_variables` in your `$HOME/.bashrc`

## 4. NEST prerequisites

1. Remove any previous system level installations of *PyNN*

        sudo apt-get remove --purge python-pynn

2. Install the GSL developer package

        sudo apt-get install libgsl0-dev

3. Install Yacc (for the MPI layer)

        sudo apt-get install bison byacc

## 5. Gzweb prerequisites

1. First make sure that build dependencies are installed.

        sudo apt-get install libgts-dev libjansson-dev

2. Install *nvm* (Node Version Manager) and *nodejs v8*

    Install nvm (steps below taken from <https://github.com/creationix/nvm>)

        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
        source ~/.bashrc # or reopen your shells

    Install node

        nvm install 8
        nvm alias default 8

3. Install bower

        npm install -g bower

## 6. NRP prerequisites

1. Configure NRP

    Go to `user-scripts` and run the configure scripts. This will set up Makefiles and default config files.

        cd "$HBP"/user-scripts
        ./configure_nrp 2>/dev/null # you don't have to answer the questions so far, just wait 5 seconds

    >**_NOTE:_**  if you get errors, like missing directories or files, this is fine, they will be fixed later in the process. Just ignore them and proceed.

2. Install dependencies

        sudo apt-get install python3-pip python3-virtualenv

        # install some deps needed for then backend (scipy deps)
        sudo apt-get install libblas-dev liblapack-dev libhdf5-dev gfortran

## 7. Install Nginx and haproxy

*Nginx* is a web server, just like apache, that is very modular and in our case serves the Frontend app and backend UWSGI app, adding an authentication layer. It also serves assets on 8090 and routes the rosbridge and gzbridge websockets. 

*Haproxy* provides a high availability load balancer and reverse proxy for TCP and HTTP-based applications. It acts as a port forwarder, dispatching traffic to frontend app, proxy and nrp-services.  

1. list existing nginx packages (```apt list --installed | grep nginx```) and remove if any
2. list existing haproxy packages (```apt list --installed | grep haproxy```) and remove if any

3. Install nginx-extras and lua-cjson and haproxy

        sudo apt-get install nginx-extras lua-cjson haproxy

    Your setup should be finished with this step. The configuration of nginx is done automatically in a later step of the 100% install. Though, if you experience issues after you finish all the steps, go to the troubleshooting section below.

4. Stop haproxy and nginx services

        sudo systemctl disable haproxy.service #This will remove haproxy from startup
        sudo service haproxy stop
        sudo service nginx stop
   
5. Install luarocks and lua-resty-http for advanced requests processing

        sudo apt-get install luarocks
        sudo luarocks install --force lua-resty-http 0.16.1


## 8. Frontend prerequisites

If you run into problems, see the Troubleshooting section at the end of the page.

1. Install dependencies:

        cd "$HBP" # your NRP directory
        cd ExDFrontend
        sudo apt-get install ruby-dev
        sudo gem install compass

2. Install grunt:

        npm install -g grunt-cli
        npm install -g grunt

## 9. Build NRP

1. Open a **fresh** terminal to be certain to have the proper environment and run the configure_nrp script in user-scripts

        cd "$HBP"/user-scripts
        ./configure_nrp  # don't answer N to the question about creating a local database (if you answer nothing, it assumes Y)

    This script copies the configuration files of *nginx*, *haproxy*, *ExDFrontend*, *CLE* as well as the *gzserver* & *gzbridge* scripts to the correct locations. It also modifies the raw config files to include the correct paths and usernames.

2. Build and reconfigure the Platform:

        ./update_nrp build --all
        ./configure_nrp

    This will take time since all the python dependencies will be downloaded and numpy takes a while to build. If the script does not fail, then you are done.
    If the build fails, go to the troubleshooting sections in the relevant sections below.

3. **Virtual machines ONLY**: if you are installing on a virtual machine, or your GPU is not supported for any reason, you may use CPU rendering, which is slower, but always works. Just execute:

        ./rendering_mode cpu

    To switch back to GPU rendering, execute:

        ./rendering_mode gpu

## 10. Install a SpiNNaker board (optional)

The SpiNNaker support software can be installed executing the following script:

```bash
$HBP/user-scripts/install_spinnaker.sh
```

To check that installation has been successful:

```bash
python -c "import pyNN.spiNNaker as sim; sim.setup(); sim.end()"
```

Ignore the error message about the config file.

Then update the `$HOME/.spynnaker.cfg` file that should have been created during the previous step. Set the 2 variables in the file as follows:

```text
machineName = 192.168.240.253
version = 3
```

If you have a SpiNNaker-5, instead use:

```text
machineName = 192.168.240.1
version = 5
```

Then configure the network interface connected to your SpiNNaker board with:

```text
IP address = 192.168.240.254
sub-mask = 255.255.255.0
default gateway = 0.0.0.0
```

Under Ubuntu, you would plug the board in, open the network manager and set up the newly detected interface with a static IPv4 setting and the addresses here up.

The configuration described here is taken from the official SpiNNaker documentation at:
<http://spinnakermanchester.github.io/spynnaker/5.0.0/PyNNOnSpinnakerInstall.html#Configuration>.

## 11. Install the fluid simulation (optional)

To install the optional fluid simulation you can follow the instructions provided here:

[Fluid simulation install guide](https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/src/master/fluid_simulation_install.md)
